<?php

namespace Drupal\batch_jobs;

use Drupal\Core\Database\Database;
use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\Core\File\FileSystemInterface;
use Drupal\media\Entity\Media;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Does the grunt work of the batch jobs.
 */
class BatchJobsService {

  /**
   * Batch process callback for field_image.
   *
   * @param int $id
   *   Id of the batch.
   * @param string $content_type
   *   Content type.
   * @param string $nids
   *   Node id's (optional).
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public function processFieldImage($id, $content_type, $nids, $operation_details, &$context) {
    $messenger = \Drupal::messenger();
    $translation = \Drupal::translation();

    // Progress message displayed.
    $log = $translation->translate('Running Batch FieldImage() - @id @details',
      ['@id' => $id, '@details' => $operation_details]
    );

    _batch_jobs_log_message($log);
    $context['message'] = $log;
    /*
    $high_water_property = _get_high_water_property($content_type);

    // Progress message displayed.
    $log = $translation->translate('Highwater property - @details',
    ['@details' => $high_water_property]
    );*/
    _batch_jobs_log_message($log);
    $context['message'] = $log;

    // Progress message displayed.
    $log = $translation->translate('NIDS - @details',
      ['@details' => print_r($nids, TRUE)]
    );
    _batch_jobs_log_message($log);
    $context['message'] = $log;

    if (!empty($nids)) {
      $nids = explode(',', $nids);
    }
    else {
      // Retrieve all nodes of this type.
      try {
        $length = 1;
        $start = ($id * $length);
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields('n', ['nid']);
        $query->condition('type', $content_type);
        $query->range($start, $length);
        $query->orderBy('nid', 'DESC');
        $nids = $query->execute()->fetchAllKeyed();
        $nids = array_keys($nids);
        // $nids = _get_nids_by_content_type($content_type, $start, $length, $high_water_property);
      } catch (\Exception $e) {
        $messenger->addMessage($translation->translate('Exception: @e', ['@e' => $e->getMessage()]));
      }
    }

    if (!empty($nids)) {
      $external_files_domain = 'https://www.topgear.com/sites/default/files/';

      foreach ($nids as $nid) {
        $node = Node::load($nid);
        if (!empty($node) & is_object($node)) {
          $node_title = $node->getTitle();

          $log = $translation->translate('Loading Nid: @nid - @title',
            ['@nid' => $nid, '@title' => $node_title]
          );
          _batch_jobs_log_message($log);

          // Get the fid from D7 DB as the D8 values could be outdated or
          // corrupt from previous migration.
          $row = _batch_jobs_get_d7_field_image_details($nid, 'node', $content_type);

          if (!empty($row['field_image_fid'])) {
            $fid = $row['field_image_fid'];
            $file = _batch_jobs_get_file_details_by_fid($fid, $id, $context);
            if (!empty($file)) {
              $alt = empty($row['field_image_alt']) ? '' : $row['field_image_alt'];
            }
            $title = $row['field_image_title'];
            $width = $row['field_image_width'];
            $height = $row['field_image_height'];

            // Create the DB entry and attached to the node.
            if (!empty($file)) {
              $fid = $file->id();
              $uri = $file->getFileUri();
              $url = str_replace('public://', $external_files_domain, $uri);

              $file = File::create([
                'uri' => $uri,
              ]);
              $file->save();

              // Overwrite the corrupted migrated entry.
              $node->field_image[0] = [
                'target_id' => $file->id(),
                'alt' => $alt,
                'title' => $title,
                'width' => $width,
                'height' => $height,
              ];

              $node->save();

              // Update progress information message.
              $log = $translation->translate('Running Batch "@id": File Added @details',
                ['@id' => $id, '@details' => $url]
              );
              _batch_jobs_log_message($log);
            }
          }
        }
      }
    }

  }

  /**
   * Batch process callback for field_carousel.
   *
   * @param int $id
   *   Id of the batch.
   * @param string $content_type
   *   Content type.
   * @param string $nids
   *   Node id's (optional).
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public function processFieldCarousel($id, $content_type, $nids, $operation_details, &$context) {
    $messenger = \Drupal::messenger();
    $translation = \Drupal::translation();

    // Progress message displayed.
    $log = $translation->translate('Running Batch processFieldCarousel() - @id @details',
      ['@id' => $id, '@details' => $operation_details]
    );

    _batch_jobs_log_message($log);
    $context['message'] = $log;

    if (!empty($nids)) {
      $nids = explode(',', $nids);
    }
    else {
      // Retrieve all nodes of this type.
      try {
        $length = 1;
        $start = ($id * $length);
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields('n', ['nid']);
        $query->condition('type', $content_type);
        $query->range($start, $length);
        $nids = $query->execute()->fetchAllKeyed();
        $nids = array_keys($nids);
      } catch (\Exception $e) {
        $messenger->addMessage($translation->translate('Exception: @e', ['@e' => $e->getMessage()]));
      }
    }

    if (!empty($nids)) {
      foreach ($nids as $nid) {
        $node = Node::load($nid);
        if (!empty($node) & is_object($node)) {
          $node_title = $node->getTitle();

          $log = $translation->translate('Loading Nid: @nid - @title',
            ['@nid' => $nid, '@title' => $node_title]
          );
          _batch_jobs_log_message($log);

          // Get the fid from D7 DB as the D8 values could be outdated or
          // corrupt from previous migration.
          $rows = _batch_jobs_get_d7_field_carousel_details($nid, 'node', $content_type);

          if (!empty($rows)) {
            foreach ($rows as $row) {
              if (!empty($fid = $row->field_carousel_fid)) {
                $delta = $row->delta;
                $description = !empty($row->field_carousel_description) ? $row->field_carousel_description : NULL;

                $file = _batch_jobs_get_file_details_by_fid($fid, $id, $context);

                // Create the DB entry and attached to the node.
                if (!empty($file)) {
                  $fid = $file->id();
                  $uri = $file->getFileUri();
                  $file = File::create([
                    'uri' => $uri,
                  ]);
                  $file->save();

                  // Overwrite the corrupted migrated entries.
                  $node->field_carousel[$delta] = [
                    'target_id' => $file->id(),
                    'display' => TRUE,
                    'description' => $description,
                  ];

                  // Update progress information message.
                  $log = $translation->translate('Running Batch "@id": File added @url',
                    ['@id' => $id, '@url' => $uri]
                  );
                  _batch_jobs_log_message($log);
                }
                else {
                  $log = $translation->translate('Running Batch "@id": File ID missing from production for NID: @nid - @fid',
                    ['@id' => $id, '@nid' => $nid, '@fid' => $fid]
                  );
                  _batch_jobs_log_message($log);

                  // Log the error.
                  \Drupal::logger('batch_jobs')
                    ->warning('File ID missing from production for NID: @nid - @fid, most likely to be a Brightcove of YouTube video',
                      ['@nid' => $nid, '@fid' => $fid]);

                  // Most likely to be a Brightcove or YouTube video,
                  // save the original fid and attached it to the node
                  // for media migration.
                  $node->field_carousel[$delta] = [
                    'target_id' => $fid,
                    'display' => TRUE,
                  ];

                }
              }
            }
            $node->save();
          }
        }
      }

    }
  }

  /**
   * Batch process callback for field_inline_gallery.
   *
   * @param int $id
   *   Id of the batch.
   * @param string $content_type
   *   Content type.
   * @param string $nids
   *   Node id's (optional).
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public function processFieldInlineGallery($id, $content_type, $nids, $operation_details, &$context) {
    $messenger = \Drupal::messenger();
    $translation = \Drupal::translation();

    // Progress message displayed.
    $log = $translation->translate('Running Batch processFieldInlineGallery() - @id @details',
      ['@id' => $id, '@details' => $operation_details]
    );

    _batch_jobs_log_message($log);
    $context['message'] = $log;

    if (!empty($nids)) {
      $nids = explode(',', $nids);
    }
    else {
      // Retrieve all nodes of this type.
      try {
        $length = 1;
        $start = ($id * $length);
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields('n', ['nid']);
        $query->condition('type', $content_type);
        $query->range($start, $length);
        $nids = $query->execute()->fetchAllKeyed();
        $nids = array_keys($nids);
      } catch (\Exception $e) {
        $messenger->addMessage($translation->translate('Exception: @e', ['@e' => $e->getMessage()]));
      }
    }

    if (!empty($nids)) {
      foreach ($nids as $nid) {
        $node = Node::load($nid);

        if (!empty($node) & is_object($node)) {
          $node_title = $node->getTitle();

          $log = $translation->translate('Loading Nid: @nid - @title',
            ['@nid' => $nid, '@title' => $node_title]
          );
          _batch_jobs_log_message($log);

          // Get the fid from D7 DB as the D8 values could be outdated or
          // corrupt from previous migration.
          $rows = _batch_jobs_get_d7_field_inline_gallery_details($nid, 'node', $content_type);
          if (!empty($rows)) {
            foreach ($rows as $row) {
              if (!empty($fid = $row->field_inline_gallery_fid)) {
                $delta = $row->delta;
                $description = !empty($row->field_inline_gallery_description) ? $row->field_inline_gallery_description : NULL;

                $file = _batch_jobs_get_file_details_by_fid($fid, $id, $context);

                // Create the DB entry and attached to the node.
                if (!empty($file)) {
                  $fid = $file->id();
                  $uri = $file->getFileUri();
                  $file = File::create([
                    'uri' => $uri,
                  ]);
                  $file->save();

                  // Overwrite the corrupted migrated entries.
                  $node->field_carousel[$delta] = [
                    'target_id' => $file->id(),
                    'display' => TRUE,
                    'description' => $description,
                  ];

                  // Update progress information message.
                  $log = $translation->translate('Running Batch "@id": File added @uri',
                    ['@id' => $id, '@uri' => $uri]
                  );
                  _batch_jobs_log_message($log);
                }
              }
            }
            $node->save();
          }
        }
      }

    }
  }

  /**
   * Batch process callback for field_inline_galleries.
   *
   * @param int $id
   *   Id of the batch.
   * @param string $content_type
   *   Content type.
   * @param string $nids
   *   Node id's (optional).
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public function processFieldInlineGalleries($id, $content_type, $nids, $operation_details, &$context) {
    $messenger = \Drupal::messenger();
    $translation = \Drupal::translation();

    // Progress message displayed.
    $log = $translation->translate('Running Batch processFieldInlineGalleries() - @id @details',
      ['@id' => $id, '@details' => $operation_details]
    );
    _batch_jobs_log_message($log);
    $context['message'] = $log;

    if (!empty($nids)) {
      $nids = explode(',', $nids);
    }
    else {
      // Retrieve all nodes of this type.
      try {
        $length = 1;
        $start = ($id * $length);
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields('n', ['nid']);
        $query->condition('type', $content_type);
        $query->range($start, $length);
        $nids = $query->execute()->fetchAllKeyed();
        $nids = array_keys($nids);
      } catch (\Exception $e) {
        $messenger->addMessage($translation->translate('Exception: @e', ['@e' => $e->getMessage()]));
      }
    }

    if (!empty($nids)) {
      foreach ($nids as $nid) {
        $node = Node::load($nid);

        if (!empty($node) & is_object($node)) {
          $node_title = $node->getTitle();

          $log = $translation->translate('Loading Nid: @nid - @title',
            ['@nid' => $nid, '@title' => $node_title]
          );
          _batch_jobs_log_message($log);

          // Get the fid from D7 DB as the D8 values could be outdated or
          // corrupt from previous migration.
          $rows = _batch_jobs_get_d7_field_inline_galleries_details($nid, 'node', $content_type);
          if (!empty($rows)) {
            // There are some missing/corrupt entries, so clear them out.
            $node->field_inline_galleries = [];
            $node->save();

            $files = [];
            foreach ($rows as $row) {
              if (!empty($fid = $row->fid)) {
                $file = _batch_jobs_get_file_details_by_fid($fid, $id, $context);
                // Create the DB entry and attached to the node.
                if (!empty($file)) {
                  $fid = $file->id();
                  $uri = $file->getFileUri();
                  $file = File::create([
                    'uri' => $uri,
                    'alt' => $node_title,
                  ]);
                  $file->save();

                  $files[] = ['target_id' => $file->id(), 'alt' => $node_title];

                  $log = $translation->translate('SAVING FILE: @nid - @title',
                    ['@nid' => $nid, '@title' => $uri]
                  );
                  _batch_jobs_log_message($log);

                  // Update progress information message.
                  $log = $translation->translate('Running Batch "@id": File added @uri',
                    ['@id' => $id, '@uri' => $uri]
                  );
                  _batch_jobs_log_message($log);
                }
              }
            }

            // If there are images, create the paragraph(s) and attached to
            // the node.
            if (!empty($files)) {
              $paragraph = Paragraph::create([
                'type' => 'inline_galleries',
                'field_images' => $files,
              ]);

              $paragraph->save();
              $node->field_inline_galleries[] = [
                'target_id' => $paragraph->id(),
                'target_revision_id' => $paragraph->getRevisionId(),
              ];

              $node->save();
            }
          }
        }
      }

    }
  }

  /**
   * Batch process callback for field_items paragraph type of News Listicles.
   *
   * @param int $id
   *   Id of the batch.
   * @param string $content_type
   *   Content type.
   * @param string $nids
   *   Node id's (optional).
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public function processFieldItems($id, $content_type, $nids, $operation_details, &$context) {
    $messenger = \Drupal::messenger();
    $translation = \Drupal::translation();

    // Progress message displayed.
    $log = $translation->translate('Running Batch processFieldItems() - @id @details',
      ['@id' => $id, '@details' => $operation_details]
    );
    _batch_jobs_log_message($log);
    $context['message'] = $log;

    if (!empty($nids)) {
      $nids = explode(',', $nids);
    }
    else {
      // Retrieve all nodes of this type.
      try {
        $length = 1;
        $start = ($id * $length);
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields('n', ['nid']);
        $query->condition('type', $content_type);
        $query->range($start, $length);
        $nids = $query->execute()->fetchAllKeyed();
        $nids = array_keys($nids);
      } catch (\Exception $e) {
        $messenger->addMessage($translation->translate('Exception: @e', ['@e' => $e->getMessage()]));
      }
    }

    if (!empty($nids)) {
      foreach ($nids as $nid) {
        $node = Node::load($nid);

        if (!empty($node) & is_object($node)) {
          $node_title = $node->getTitle();
          $log = $translation->translate('Loading Nid: @nid - @title',
            ['@nid' => $nid, '@title' => $node_title]
          );
          _batch_jobs_log_message($log);
          // Get the fid from D7 DB as the D8 values could be outdated or
          // corrupt from previous migration.
          $rows = _batch_jobs_get_d7_field_news_listicle_items_details($nid, 'node', $content_type);
          if (!empty($rows)) {
            // There are some missing/corrupt entries, so clear them out.
            $node->field_news_listicle_items->setValue([]);
            $node->save();

            foreach ($rows as $row) {
              if (!empty($fid = $row->fid)) {
                $file = _batch_jobs_get_file_details_by_fid($fid, $id, $context);
                $image = [];
                // Create the DB entry and attached to the node.
                if (!empty($file)) {
                  $uri = $file->getFileUri();
                  $file = File::create([
                    'uri' => $uri,
                  ]);
                  $file->save();
                }

                $title = !empty($row->field_news_listicle_title_value) ? $row->field_news_listicle_title_value : '';
                $text = !empty($row->field_news_listicle_text_value) ? $row->field_news_listicle_text_value : '';
                $alt = !empty($row->field_file_image_alt_text_value) ? $row->field_file_image_alt_text_value : '';

                if (!empty($file)) {
                  $image = [
                    'target_id' => $file->id(),
                    'title' => $title,
                    'alt' => $alt,
                  ];
                }
              }

              $paragraph = Paragraph::create([
                'type' => 'news_listicle_items',
                'field_image' => $image,
                'field_news_listicle_title' => $title,
                'field_news_listicle_text' => [
                  'value' => $text,
                  'format' => 'editor',
                ],
              ]);

              $paragraph->save();

              $node->field_news_listicle_items[] = [
                'target_id' => $paragraph->id(),
                'target_revision_id' => $paragraph->getRevisionId(),
              ];

              $log = $translation->translate('SAVING FILE: @nid - @title',
                ['@nid' => $nid, '@title' => $uri]
              );
              _batch_jobs_log_message($log);

              // Update progress information message.
              $log = $translation->translate('Running Batch "@id": File added @uri',
                ['@id' => $id, '@uri' => $uri]
              );
              _batch_jobs_log_message($log);

            }

            // Re-save the node with the updated paragraph(s).
            $node->save();
          }
        }
      }

    }
  }

  /**
   * Callback for migrating file entities (items1) to media entities (items2).
   *
   * @param int $id
   *   Id of the batch.
   * @param string $content_type
   *   Content type.
   * @param string $media_field
   *   New field migrating to.
   * @param string $nids
   *   Node id's (optional).
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public function processNewsListicleItems2($id, $content_type, $media_field, $nids, $operation_details, &$context) {
    $messenger = \Drupal::messenger();
    $translation = \Drupal::translation();

    // Progress message displayed.
    $log = $translation->translate('Running Batch processNewsListicleItems2() - @id @details',
      ['@id' => $id, '@details' => $operation_details]
    );
    _batch_jobs_log_message($log);
    $context['message'] = $log;

    if (!empty($nids)) {
      $nids = explode(',', $nids);
    }
    else {
      // Retrieve all nodes of this type.
      try {
        $length = 1;
        $start = ($id * $length);
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields('n', ['nid']);
        $query->condition('type', $content_type);
        $query->range($start, $length);
        $nids = $query->execute()->fetchAllKeyed();
        $nids = array_keys($nids);
      } catch (\Exception $e) {
        $messenger->addMessage($translation->translate('Exception: @e', ['@e' => $e->getMessage()]));
      }
    }

    if (!empty($nids)) {
      foreach ($nids as $nid) {
        $node = Node::load($nid);

        if (!empty($node) & is_object($node)) {
          $nid = $node->id();

          if (
          $node->hasField($media_field)
          ) {
            // First check if the media entities already exist as they may
            // have been migrated from items1 (paragraphs).
            // If they exists then we don't have run this process of converting
            // D7 items2 into media entities as they didn't exist.
            $media_images = $node->get($media_field)->getValue();

            $log = $translation->translate('Media Image count @count for nid @nid',
              ['@count' => count($media_images), '@nid' => $nid]
            );
            _batch_jobs_log_message($log);

            if (!empty($media_images)) {
              $log = $translation->translate('@count media entities already created for nid @nid',
                ['@count' => count($media_images), '@nid' => $nid]
              );
              _batch_jobs_log_message($log);

              break;
            }

            // Get the fid from D7 DB as the D8 values could be outdated or
            // corrupt from previous migration.
            $rows = _batch_jobs_get_d7_field_news_listicle_items2_details($nid);

            $media_images = $node->get($media_field)->getValue();

            // To avoid possible duplication, delete all the entities attached
            // to the node and re-created them.
            $ids = [];
            if (!empty($media_images)) {
              foreach ($media_images as $key => $value) {
                $ids[$key] = $value['target_id'];
              }

              $storage_handler = \Drupal::entityTypeManager()
                ->getStorage('media');
              $entities = $storage_handler->loadMultiple($ids);

              $storage_handler->delete($entities);
            }

            if (count($rows) != count($ids)) {
              foreach ($rows as $row) {
                $fid = $row->field_news_listicle_items2_fid;
                $file = _batch_jobs_get_file_details_by_fid($fid, $id, $context);

                if (!empty($file)) {
                  $name = !empty($row->filename) ? $row->filename : '';

                  $title = !empty($row->field_file_image_title_text_value) ? $row->field_file_image_title_text_value : '';
                  $text = !empty($row->field_image_body_text_value) ? $row->field_image_body_text_value : '';
                  $alt = !empty($row->field_file_image_alt_text_value) ? $row->field_file_image_alt_text_value : '';

                  $media_entity = Media::create([
                    'bundle' => 'image',
                    'uid' => '1',
                    'langcode' => \Drupal::languageManager()
                      ->getDefaultLanguage()
                      ->getId(),
                    'name' => $name,
                    'status' => 1,
                    'created' => date('U'),
                    'changed' => date('U'),
                    'moderation_state' => 'published',
                    'thumbnail' => [
                      'target_id' => $file->id(),
                      'alt' => $alt,
                      'title' => $title,
                    ],
                    // Not to be confused with the media field attached
                    // to the content type, but the field used in
                    // the image media entity.
                    'field_image' => [
                      'target_id' => $file->id(),
                      'alt' => $alt,
                      'title' => $title,
                    ],
                    // Applicable for news listicle items2.
                    'field_file_image_title_text' => $title,
                    'field_image_body_text' => [
                      'value' => $text,
                      'format' => 'editor',
                    ],
                  ]);
                  $media_entity->save();

                  // Attached to the node media field.
                  $node->get($media_field)->appendItem($media_entity);

                  // Update progress information message.
                  $log = $translation->translate('Running Batch "@id":
                  File Downloaded for @details',
                    ['@id' => $id, '@details' => $node->get('title')->value]
                  );
                  _batch_jobs_log_message($log);

                }
                else {
                  // Possible Brightcove or YouTube video.
                  $uri = _batch_jobs_get_external_url($file->id());
                  if (!empty($uri)) {
                    switch (explode('://', $uri)[0]) {
                      case 'brightcove':

                        $log = $translation->translate('Possible Brightcove vid: @fid',
                          ['@fid' => $file->id()]
                        );
                        _batch_jobs_log_message($log);

                        $uri = _batch_jobs_get_external_url($file->id());
                        $video_id = _batch_jobs_brightcove_video_id($uri);
                        $brightcove_id = get_brightcove_id($video_id);
                        // Get name from D8 Brightcove table.
                        $name = Database::getConnection()
                          ->query('SELECT name FROM {brightcove_video} WHERE bcvid = :bcvid', ['bcvid' => $brightcove_id])
                          ->fetchField();
                        $media_entity = Media::create([
                          'bundle' => 'brightcove_video',
                          'name' => $name,
                          'uid' => '1',
                          'status' => TRUE,
                          'moderation_state' => 'published',
                          'field_media_brightcove_video' => ['target_id' => $brightcove_id],
                        ]);
                        $media_entity->save();

                        // Attached to the node media field.
                        $node->get($media_field)->appendItem($media_entity);
                        break;

                      case
                      'youtube':
                        $log = $translation->translate('Possible YouTube vid: @fid',
                          ['@fid' => $file->id()]
                        );
                        _batch_jobs_log_message($log);

                        $url = str_replace('youtube://v/', 'https://www.youtube.com/watch?v=', $uri);

                        // Load or create media entity for URL:
                        $entityIds = \Drupal::entityQuery('media')
                          ->condition('field_media_oembed_video.value', $url)
                          ->execute();
                        if (!empty($entityIds)) {
                          $media_entity = Media::load(array_keys($entityIds)[0]);
                        }
                        else {
                          $media_entity = Media::create([
                            'bundle' => 'youtube',
                            'uid' => '1',
                            'status' => TRUE,
                            'moderation_state' => 'published',
                            'field_media_oembed_video' => ['value' => $url],
                          ]);
                          $media_entity->save();
                        }

                        // Attached to the node media field.
                        $node->get($media_field)->appendItem($media_entity);
                        break;
                    }
                  }
                }
              }
            }
            $node->save();
          }
        }

      }
    }
  }

  /**
   * Batch process callback for migrating file entities to media entities.
   *
   * @param int $id
   *   Id of the batch.
   * @param string $content_type
   *   Content type.
   * @param string $original_field
   *   Original field migrating from.
   * @param string $media_field
   *   New field migrating to.
   * @param string $nids
   *   Node id's (optional).
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public function processMediaEntities($id, $content_type, $original_field, $media_field, $nids, $operation_details, &$context) {
    $messenger = \Drupal::messenger();
    $translation = \Drupal::translation();

    // Progress message displayed.
    $log = $translation->translate('Running Batch processMediaEntities() - @id @details',
      ['@id' => $id, '@details' => $operation_details]
    );
    _batch_jobs_log_message($log);
    $context['message'] = $log;

    if (!empty($nids)) {
      $nids = explode(',', $nids);
    }
    else {
      // Retrieve all nodes of this type.
      try {
        $length = 1;
        $start = ($id * $length);
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields('n', ['nid']);
        $query->condition('type', $content_type);
        $query->range($start, $length);
        $nids = $query->execute()->fetchAllKeyed();
        $nids = array_keys($nids);
      } catch (\Exception $e) {
        $messenger->addMessage($translation->translate('Exception: @e', ['@e' => $e->getMessage()]));
      }
    }

    if (!empty($nids)) {
      foreach ($nids as $nid) {
        $node = Node::load($nid);

        if (!empty($node) & is_object($node)) {
          $nid = $node->id();
          $node_title = $node->getTitle();
          $log = $translation->translate('Loading Nid: @nid - @title',
            ['@nid' => $nid, '@title' => $node_title]
          );
          _batch_jobs_log_message($log);

          if (
            $node->hasField($original_field) &&
            $node->hasField($media_field)
          ) {
            $images = $node->get($original_field)->getValue();

            // If the content type is news listicle and the field is a
            // news listicle item. Then load the paragraph(s)
            // to get the file ids.
            if ($content_type == 'news_listicle' && $original_field == 'field_news_listicle_items') {
              $images = [];
              $paragraphs = $node->get($original_field)->getValue();
              // Loop through the result set.
              foreach ($paragraphs as $element) {
                $paragraph = Paragraph::load($element['target_id']);
                if (!empty($paragraph)) {
                  $image = NULL;
                  if (!empty($paragraph->field_image)) {
                    $image = $paragraph->field_image->getValue();
                    $title = $image[0]['title'];
                    $alt = $image[0]['alt'];

                    $log = $translation->translate('Image details - @image',
                      ['@image' => print_r($image, TRUE)]
                    );
                    _batch_jobs_log_message($log);

                    $image = $image[0]['target_id'];
                  }
                  if (!empty($paragraph->field_news_listicle_title)) {
                    $title = $paragraph->field_news_listicle_title->getValue();
                  }
                  if (!empty($paragraph->field_news_listicle_text)) {
                    $text = $paragraph->field_news_listicle_text->getValue();
                  }

                  $images[] = [
                    'target_id' => $image,
                    'title' => $title,
                    'alt' => $alt,
                    'text' => $text,
                  ];
                }
              }
            }

            $media_images = $node->get($media_field)->getValue();

            $log = $translation->translate('Image count @count',
              ['@count' => count($images)]
            );
            _batch_jobs_log_message($log);

            $log = $translation->translate('Media Image count @count',
              ['@count' => count($media_images)]
            );
            _batch_jobs_log_message($log);

            // To avoid possible duplication, delete all the entities attached
            // to the node and re-created them.
            if (!empty($media_images)) {
              $ids = [];
              foreach ($media_images as $key => $value) {
                $ids[$key] = $value['target_id'];
              }

              $storageHandler = \Drupal::entityTypeManager()
                ->getStorage('media');
              $entities = $storageHandler->loadMultiple($ids);
              $storageHandler->delete($entities);

              $node->set($media_field, []);
            }

            foreach ($images as $image) {
              $file = File::load($image['target_id']);

              if (!empty($file)) {
                $name = !empty($file->getFilename()) ? $file->getFilename() : '';

                $title = !empty($image['title']) ? $image['title'] : '';
                $alt = !empty($image['alt']) ? $image['alt'] : '';
                $text = !empty($image['text']) ? $image['text'] : '';

                $media_entity = Media::create([
                  'bundle' => 'image',
                  'uid' => '1',
                  'langcode' => \Drupal::languageManager()
                    ->getDefaultLanguage()
                    ->getId(),
                  'name' => $name,
                  'status' => 1,
                  'created' => date('U'),
                  'changed' => date('U'),
                  'moderation_state' => 'published',
                  'thumbnail' => [
                    'target_id' => $file->id(),
                    'alt' => $alt,
                    'title' => $title,
                  ],
                  // Not to be confused with the media field attached
                  // to the content type, but the field used in
                  // the image media entity.
                  'field_image' => [
                    'target_id' => $file->id(),
                    'alt' => $alt,
                    'title' => $title,
                  ],
                  // Applicable for news listicle items.
                  'field_file_image_title_text' => $title,
                  'field_image_body_text' => $text,
                ]);
                $media_entity->save();

                // Attached to the node media field.
                $node->get($media_field)->appendItem($media_entity);

                // Update progress information message.
                $log = $translation->translate('Running Batch "@id":
                  File Downloaded for @details',
                  ['@id' => $id, '@details' => $node->get('title')->value]
                );
                _batch_jobs_log_message($log);

              }
              else {
                // Possible Brightcove or YouTube video.
                $uri = _batch_jobs_get_external_url($image['target_id']);
                if (!empty($uri)) {
                  switch (explode('://', $uri)[0]) {
                    case 'brightcove':
                      $log = $translation->translate('Possible Brightcove vid: @fid',
                        ['@fid' => $image['target_id']]
                      );
                      _batch_jobs_log_message($log);

                      $uri = _batch_jobs_get_external_url($image['target_id']);
                      $video_id = _batch_jobs_brightcove_video_id($uri);
                      $brightcove_id = get_brightcove_id($video_id);

                      // When syncing data from the Brightcove API, the player assigned to
                      // the video is not imported. Using the $brightcove_details above
                      // Assign the player ID to the video file.
                      // Not every video has a player assigned to it (null)
                      // and in some cases the player is set to "_none",
                      // if this is the case, use the "TG default player".
                      // Get the Brightcove player ID.
                      $brightcove_player_id = get_brightcove_player_by_video_id($video_id);
                      if (empty($brightcove_player_id) || $brightcove_player_id == '_none') {
                        $brightcove_player_id = '80a1b445-45ef-49d2-a70e-d9611e7b5231';
                      }
                      // We need the internal brightcove player id (bcpid),
                      // not brightcove's actual player ID from their API...
                      // ....because it's not complicated enough.
                      $player_id = get_brightcove_internal_player_id($brightcove_player_id);

                      // Set the brightcove video with the correct player ID.
                      set_brightcove_video_player($brightcove_id, $player_id);
                      // If video content type, assign the brightcove id to
                      // to the Brightcove entity.
                      if ($content_type == 'videos_video') {
                        // Update the node with the brightcove id (internal video id).
                        $node->get('field_brightcove_video')
                          ->setValue($brightcove_id);
                        $node->save();
                        break;
                      }

                      // Get name from D8 Brightcove table.
                      $name = Database::getConnection()
                        ->query('SELECT name FROM {brightcove_video} WHERE bcvid = :bcvid', ['bcvid' => $brightcove_id])
                        ->fetchField();
                      $media_entity = Media::create([
                        'bundle' => 'brightcove_video',
                        'name' => $name,
                        'uid' => '1',
                        'status' => TRUE,
                        'moderation_state' => 'published',
                        'field_media_brightcove_video' => ['target_id' => $brightcove_id],
                      ]);
                      $media_entity->save();

                      // Attached to the node media field.
                      $node->get($media_field)->appendItem($media_entity);
                      break;

                    case
                    'youtube':
                      $log = $translation->translate('Possible YouTube vid: @fid',
                        ['@fid' => $image['target_id']]
                      );
                      _batch_jobs_log_message($log);

                      $url = str_replace('youtube://v/', 'https://www.youtube.com/watch?v=', $uri);

                      // Load or create media entity for URL:
                      $entityIds = \Drupal::entityQuery('media')
                        ->condition('field_media_oembed_video.value', $url)
                        ->execute();
                      if (!empty($entityIds)) {
                        $media_entity = Media::load(array_keys($entityIds)[0]);
                      }
                      else {
                        $media_entity = Media::create([
                          'bundle' => 'youtube',
                          'uid' => '1',
                          'status' => TRUE,
                          'moderation_state' => 'published',
                          'field_media_oembed_video' => ['value' => $url],
                        ]);
                        $media_entity->save();
                      }

                      // Attached to the node media field.
                      $node->get($media_field)->appendItem($media_entity);
                      break;
                  }
                }
              }
            }
            $node->save();
          }

        }
      }
    }
    // When indexing via Drush, multiple iterations of a batch will happen in
    // the same PHP process, so the static cache will quickly fill up. To
    // prevent this, clear it after each batch of items gets indexed.
    if (function_exists('drush_backend_batch_process') && batch_get()) {
      \Drupal::getContainer()->get('entity.memory_cache')->deleteAll();
    }
  }

  /**
   * Batch process callback.
   *
   * @param int $id
   *   Id of the batch.
   * @param string $content_type
   *   Content type.
   * @param string $nids
   *   Node id's (optional).
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public function syncNodesStatus($id, $content_type, $nids, $operation_details, &$context) {
    $messenger = \Drupal::messenger();
    $translation = \Drupal::translation();

    // Progress message displayed.
    $log = $translation->translate('Running Batch "@id" @details',
      ['@id' => $id, '@details' => $operation_details]
    );
    _batch_jobs_log_message($log);
    $context['message'] = $log;

    if (!empty($nids)) {
      $nids = explode(',', $nids);
    }
    else {
      // Retrieve 100 nodes at a time.
      try {
        $length = 1;
        $start = ($id * $length);
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields('n', ['nid']);
        if (!empty($content_type)) {
          $query->condition('type', $content_type);
        }
        $query->range($start, $length);
        $nids = $query->execute()->fetchAllKeyed();
        $nids = array_keys($nids);
      } catch (\Exception $e) {
        $messenger->addMessage($translation->translate('Exception: @e', ['@e' => $e->getMessage()]));
      }
    }

    if (!empty($nids)) {
      \Drupal::entityTypeManager()->getStorage('node')->resetCache($nids);
      $nodes = Node::loadMultiple($nids);
      foreach ($nodes as $node) {
        if (!empty($node) & is_object($node)) {
          // Check node status from D7 DB.
          $nid = $node->id();

          $status = _batch_jobs_get_node_status($nid);
          $node_status = FALSE;
          // $node->set('moderation_state', 'draft');
          if ($status == 1) {
            $node_status = TRUE;
            // $node->set('moderation_state', 'published');
          }

          $node = $node->setPublished($node_status);
          $node->save();
        }
      }
    }
  }

  /**
   * Batch process callback.
   *
   * @param int $id
   *   Id of the batch.
   * @param string $content_type
   *   Content type.
   * @param string $nids
   *   Node id's (optional).
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public function processContentType($id, $content_type, $nids, $operation_details, &$context) {
    $messenger = \Drupal::messenger();
    $translation = \Drupal::translation();

    // Progress message displayed.
    $log = $translation->translate('Running Batch processContentType() - @id @details',
      ['@id' => $id, '@details' => $operation_details]
    );
    _batch_jobs_log_message($log);
    $context['message'] = $log;

    if (!empty($nids)) {
      // We need to split these nids in a single array as batching the job
      // one at a time due to memory issues.
      $nids = explode(',', $nids);
      $nids = array_values($nids);
      $nids = $nids[$id - 1];
      $nids = [$nids];

      $log = $translation->translate('NIDS: @nid',
        ['@nid' => print_r($nids, TRUE)],
      );
      _batch_jobs_log_message($log);
    }
    else {
      // Retrieve 100 nodes at a time.
      try {
        $length = 1;
        $start = ($id * $length);
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields('n', ['nid']);
        if (!empty($content_type)) {
          $query->condition('type', $content_type);
        }
        $query->range($start, $length);
        $nids = $query->execute()->fetchAllKeyed();
        $nids = array_keys($nids);

        $log = $translation->translate('NIDS: @nid',
          ['@nid' => print_r($nids, TRUE)],
        );
        _batch_jobs_log_message($log);
      } catch (\Exception $e) {
        $messenger->addMessage($translation->translate('Exception: @e', ['@e' => $e->getMessage()]));
      }
    }
    if (!empty($nids)) {
      \Drupal::entityTypeManager()->getStorage('node')->resetCache($nids);
      $nodes = Node::loadMultiple($nids);
      foreach ($nodes as $node) {
        if (!empty($node) & is_object($node)) {
          $nid = $node->id();
          $node_title = $node->getTitle();

          $log = $translation->translate('Loading Nid: @nid - @title',
            ['@nid' => $nid, '@title' => $node_title]
          );
          _batch_jobs_log_message($log);
          $context['message'] = $log;

          $service = new BatchJobsService();

          switch ($content_type) {
            case 'news_article':
            case 'big_read':
              $service->processFieldImage($id, $content_type, $nid, $operation_details, $context);
              $service->processFieldCarousel($id, $content_type, $nid, $operation_details, $context);
              $service->processMediaEntities($id, $content_type, 'field_big_read_media', 'field_big_read_media', $nid, $operation_details, $context);
              $service->processMediaEntities($id, $content_type, 'field_carousel', 'field_carousel_media', $nid, $operation_details, $context);
              $service->processFieldInlineGalleries($id, $content_type, $nid, $operation_details, $context);
              $service->syncNodesStatus($id, $content_type, $nid, $operation_details, $context);
              break;

            case 'cars_road_test':
            case 'cars_car':
              $service->processFieldImage($id, $content_type, $nid, $operation_details, $context);
              $service->processFieldCarousel($id, $content_type, $nid, $operation_details, $context);
              $service->processMediaEntities($id, $content_type, 'field_carousel', 'field_carousel_media', $nid, $operation_details, $context);
              $service->processFieldInlineGallery($id, $content_type, $nid, $operation_details, $context);
              $service->syncNodesStatus($id, $content_type, $nid, $operation_details, $context);
              break;

            case 'cars_garage':
              $service->processFieldImage($id, $content_type, $nid, $operation_details, $context);
              $service->processFieldCarousel($id, $content_type, $nid, $operation_details, $context);
              $service->processMediaEntities($id, $content_type, 'field_carousel', 'field_carousel_media', $nid, $operation_details, $context);
              $service->syncNodesStatus($id, $content_type, $nid, $operation_details, $context);
              break;

            case 'videos_video':
              $service->processFieldImage($id, $content_type, $nid, $operation_details, $context);
              $service->processMediaEntities($id, $content_type, 'field_videos_video', 'field_brightcove_video', $nid, $operation_details, $context);
              $service->syncNodesStatus($id, $content_type, $nid, $operation_details, $context);
              break;

            case 'news_listicle':
              // Progress message displayed.
              $service->processFieldImage($id, $content_type, $nid, $operation_details, $context);

              // Editors are still using Items1 and Item2, so we need to check
              // for both and migrate them over.
              $service->processFieldItems($id, $content_type, $nid, $operation_details, $context);
              $service->processMediaEntities($id, $content_type, 'field_news_listicle_items', 'field_news_listicle_items2', $nid, $operation_details, $context);
              $service->processNewsListicleItems2($id, $content_type, 'field_news_listicle_items2', $nid, $operation_details, $context);

              $service->updateNewsListicleDisplayFormat($id, $content_type, $nid, $operation_details, $context);
              $service->syncNodesStatus($id, $content_type, $nid, $operation_details, $context);
              break;
          }

          $log = $translation->translate('Saving Nid: @nid - @title',
            ['@nid' => $nid, '@title' => $node_title]
          );
          _batch_jobs_log_message($log);
          $context['message'] = $log;
        }
      }
    }
  }

  /**
   * Batch process callback.
   *
   * @param int $id
   *   Id of the batch.
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public function downloadMissingImages($id, $operation_details, &$context) {
    $messenger = \Drupal::messenger();
    $translation = \Drupal::translation();

    $external_files_domain = 'https://www.topgear.com/sites/default/files/';

    // Retrieve all nodes of this type.
    try {
      $length = 100;
      $start = ($id * $length);
      $db = _batch_jobs_connect_to_migration_db();
      $query = $db->select('file_managed', 'fm');
      $query->fields('fm', ['uri']);
      $query->orderBy('timestamp', 'DESC');
      $query->range($start, $length);
      $uris = $query->execute()->fetchAllKeyed();
    } catch (\Exception $e) {
      $messenger->addMessage($translation->translate('Exception: @e', ['@e' => $e->getMessage()]));
    }

    $uris = array_keys($uris);
    foreach ($uris as $uri) {
      // Generate external URL to grab the file.
      $url = str_replace('public://', $external_files_domain, $uri);

      // Get the file name.
      $file_name = basename($url);

      $log = $translation->translate('Running Batch "@id": Checking for file @uri',
        ['@id' => $id, '@uri' => $uri]
      );
      _batch_jobs_log_message($log);

      // Check if the file is missing on this server.
      if (!file_exists($uri)) {
        // If missing, check if it's actually available on prod.
        // If so, download it.
        $file_exists = _batch_jobs_check_external_file($url);
        if ($file_exists == 200) {
          // Generate the public directory.
          $dir = str_replace($external_files_domain, '', $url);
          $dir = dirname($dir);
          $public_dir = 'public://' . $dir . '/';

          // Create the directory if needed and download the file.
          $fileSystem = \Drupal::service('file_system');
          if ($fileSystem->prepareDirectory($public_dir, FileSystemInterface::CREATE_DIRECTORY)) {
            try {
              $log = $translation->translate('Running Batch "@id": Downloading file @url',
                ['@id' => $id, '@url' => $url]
              );
              _batch_jobs_log_message($log);

              $file = system_retrieve_file($url, $public_dir . $file_name, TRUE, TRUE);
            } catch (Exception $e) {
              $log = $translation->translate('Running Batch "@id": Exception @"exception"',
                ['@id' => $id, '@exception' => $e->getMessage()]
              );
              _batch_jobs_log_message($log);
              $messenger->addMessage($e->getMessage());
            }

            // If the file was download now create the DB entry
            // and attached it to the node.
            if (!empty($file)) {
              $log = $translation->translate('Running Batch "@id": File Downloaded @url',
                ['@id' => $id, '@url' => $url]
              );
              _batch_jobs_log_message($log);

            }
          }
        }
        else {
          $log = $translation->translate('Running Batch "@id": File Missing from production @url',
            ['@id' => $id, '@url' => $url]
          );
          _batch_jobs_log_message($log);

          // Log the error.
          \Drupal::logger('batch_jobs')
            ->info('File Missing from production @url',
              [
                '@url' => $url,
              ]);
        }
      }
    }
  }

  /**
   * Batch process callback.
   *
   * @param int $id
   *   Id of the batch.
   * @param string $content_type
   *   Content type.
   * @param string $nids
   *   Node id's (optional).
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public function updateNewsListicleDisplayFormat($id, $content_type, $nids, $operation_details, &$context) {
    $messenger = \Drupal::messenger();
    $translation = \Drupal::translation();

    // Progress message displayed.
    $log = $translation->translate('Running Batch updateNewsListicleDisplayFormat() - @id @details',
      ['@id' => $id, '@details' => $operation_details]
    );
    _batch_jobs_log_message($log);
    $context['message'] = $log;

    if (!empty($nids)) {
      $nids = explode(',', $nids);
    }
    else {
      // Retrieve 100 nodes at a time.
      try {
        $length = 1;
        $start = ($id * $length);
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields('n', ['nid']);
        if (!empty($content_type)) {
          $query->condition('type', $content_type);
        }
        $query->range($start, $length);
        $nids = $query->execute()->fetchAllKeyed();
        $nids = array_keys($nids);
      } catch (\Exception $e) {
        $messenger->addMessage($translation->translate('Exception: @e', ['@e' => $e->getMessage()]));
      }
    }

    if (!empty($nids)) {
      \Drupal::entityTypeManager()->getStorage('node')->resetCache($nids);
      $nodes = Node::loadMultiple($nids);
      foreach ($nodes as $node) {
        if (!empty($node) & is_object($node)) {
          $nid = $node->id();

          $display_format = _get_news_listicle_display_format($content_type, $nid);
          // Some Listicles don't have a value set, so default to normal format.
          $display_format = empty($display_format) ?? 'normal';

          $node->field_news_listicle_format = $display_format;
          $node->save();
        }
      }
    }
  }


  /**
   * Batch process callback.
   *
   * @param int $id
   *   Id of the batch.
   * @param string $content_type
   *   Content type.
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public function processMissingNids($id, $content_types, $operation_details, &$context) {
    $messenger = \Drupal::messenger();
    $translation = \Drupal::translation();

    // Progress message displayed.
    $log = $translation->translate('Running Batch processMissingNids() - @id @details',
      ['@id' => $id, '@details' => $operation_details]
    );
    _batch_jobs_log_message($log);
    $context['message'] = $log;

    foreach ($content_types as $content_type) {
      $nids = _batch_jobs_get_missing_nids($content_type);


    }


    if (!empty($nids)) {
      \Drupal::entityTypeManager()->getStorage('node')->resetCache($nids);
      $nodes = Node::loadMultiple($nids);
      foreach ($nodes as $node) {
        if (!empty($node) & is_object($node)) {
          $nid = $node->id();

          $display_format = _get_news_listicle_display_format($content_type, $nid);
          // Some Listicles don't have a value set, so default to normal format.
          $display_format = empty($display_format) ?? 'normal';

          $node->field_news_listicle_format = $display_format;
          $node->save();
        }
      }
    }
  }

  /**
   * Batch Finished callback.
   *
   * @param bool $success
   *   Success of the operation.
   * @param array $results
   *   Array of results for post processing.
   * @param array $operations
   *   Array of operations.
   *
   *   Currently used for debug info. May come back to see if the output
   *   can be formatted in a more useful way, right now it'll do.
   */
  public function batchJobFinished($success, array $results, array $operations) {
    $messenger = \Drupal::messenger();
    $translation = \Drupal::translation();

    $messenger->addMessage($translation->translate('@results',
      ['@results' => print_r($results, TRUE)]));
    if ($success) {
      $messenger->addMessage($translation->translate('Batch job complete'));
      if (!empty($results['not_found'])) {
        $messenger->addMessage($translation->translate('The following files are missing from the server'));
        $messenger->addMessage(_batch_jobs_get_page_render($results['not_found_header'], $results['not_found']));
      }
      if (!empty($results['results'])) {
        // Display the number of nodes we processed.
        $messenger->addMessage($translation->translate('@count results processed.', ['@count' => count($results['results'])]));
        $messenger->addMessage($translation->translate('The following node(s) have been updated:'));
        $messenger->addMessage(_batch_jobs_get_page_render($results['header'], $results['results']));
        return new RowsOfFields($results['header']);
      }
    }
    else {
      $error_operation = reset($operations);
      $messenger->addMessage(
        $translation->translate('An error occurred while processing @operation with arguments : @args',
          [
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE),
          ]
        )
      );
    }
  }

}
