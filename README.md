----------------------------------------------------------------------------------------------------------------------------------------------------------

Batch shell script:

run `sh tg_migrate_import.sh` from this modules root folder. It will run all the migrations in specific order via drush,
which will give back more information compared to using manifest file.

----------------------------------------------------------------------------------------------------------------------------------------------------------

The patches are failing when trying to apply via composer, so these must be applied manually.

Apply these outside of the container and run from inside the patches directory.

patch '-p1' --no-backup-if-mismatch -d '../../../../modules/contrib/paragraphs' < '2911244-80.paragraphs.Field-collections-deriver-and-base-migration.patch'

patch '-p1' --no-backup-if-mismatch -d '../../../../modules/contrib/field_group' < '2951335-7-fix-core-8-5-fieldgroup-migration.patch'

patch '-p1' --no-backup-if-mismatch -d '../../../../' < '2996114-124.patch'

# Compatibility break with graphql 3.0 (Metatags 1.13)
patch '-p1' --no-backup-if-mismatch -d '../../../../modules/contrib/metatag' < 'metatag-n3129863-3.patch'

# Support JSON API, REST, GraphQL and custom normalizations
patch '-p1' --no-backup-if-mismatch -d '../../../../modules/contrib/metatag' < '2945817-93.patch'
