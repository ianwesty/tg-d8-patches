<?php

namespace Drupal\batch_jobs\Commands;

use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drush\Commands\DrushCommands;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * A Drush commandfile.
 */
class BatchJobsDrushCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * Entity type service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * Constructs a new UpdateVideosStatsController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->entityTypeManager = $entityTypeManager;
    $this->loggerChannelFactory = $loggerChannelFactory;
  }

  /**
   * Prepare batch job for field_image.
   *
   * @param string $content_type
   *   Content type to update.
   * @param bool $force
   *   Ignore high water property and force the update.
   * @param string $nids
   *   Comma separated file of Node id's.
   *
   * @command update:field-image
   * @aliases update-field-image
   *
   * @options string $force
   *   Ignore high water property and force the update.
   * @options string $nids
   *   Comma separated file of Node id's.
   *
   * @usage update-field-carousel content_type force nid(s)
   *   content_type is the type of node to update
   *   Ignore high water property and force the update.
   *   Comma separated file of Node id's.
   */
  public function updateFieldImage($content_type, $force = FALSE, $nids = '') {
    // Log the start of the script.
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update nodes batch operations start');
    $batch_number = 1;
    if (!empty($nids)) {
      $count = count(explode(',', $nids));
    }
    else {
      // Retrieve all nodes of this type.
      try {
        $high_water_property = _get_high_water_property($content_type);

        $this->loggerChannelFactory->get('batch_jobs')
          ->info('High water prop: ' . $high_water_property);

        $nids = _get_nids_by_content_type($content_type, 0, $batch_number, $high_water_property);
        $count = count($nids);
        $nids = implode(',', $nids);
      }
      catch (\Exception $e) {
        $this->output()->writeln($e);
        $this->loggerChannelFactory->get('batch_jobs')
          ->warning('Error found @e', ['@e' => $e]);
      }
    }

    // Create the operations array for the batch.
    $batch_operations = 0;
    $batch_id = 1;

    $num_operations = ceil($count / $batch_number);
    $operations = [];

    // Running through $num_operations amount of times
    // and run 100 nodes per batch per.
    for ($i = 0; $i < $num_operations; $i++) {
      // Prepare the operation. Here we could do other operations on nodes.
      $this->output()->writeln("Preparing batch: " . $batch_id);

      $operations[] = [
        '\Drupal\batch_jobs\BatchJobsService::processFieldImage',
        [
          $batch_id,
          $content_type,
          $nids,
          $this->t('Updating batch @batch', ['@batch' => $batch_id]),
        ],
      ];
      $batch_id++;
      $batch_operations++;
    }

    // Create the batch.
    $batch = [
      'title' => $this->t('Updating @num node(s)', ['@num' => $batch_operations]),
      'operations' => $operations,
      'finished' => '\Drupal\batch_jobs\BatchJobsService::batchJobFinished',
    ];

    $this->logger()->warning("Total Nodes: " . $count);

    // Add batch operations as new batch sets.
    batch_set($batch);

    // Process the batch sets.
    drush_backend_batch_process();

    // Log batch end notification.
    $this->logger()->notice("Batch operations end.");
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update batch operations end.');
  }

  /**
   * Prepare batch job for field_carousel.
   *
   * @param string $content_type
   *   Content type to update.
   * @param string $nids
   *   Comma separated file of Node id's.
   *
   * @command update:field-carousel
   * @aliases update-field-carousel
   *
   * @options string $nids
   *   Comma separated file of Node id's.
   *
   * @usage update:field-carousel content_type
   *   content_type is the type of node to update
   */
  public function updateFieldCarousel($content_type, $nids = '') {
    // Log the start of the script.
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update nodes batch operations start');

    if (!empty($nids)) {
      $count = count(explode(',', $nids));
    }
    else {
      // Retrieve all nodes of this type.
      try {
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields(NULL, ['nid']);
        $query->condition('type', $content_type);
        $count = $query->countQuery()->execute()->fetchField();
      }
      catch (\Exception $e) {
        $this->output()->writeln($e);
        $this->loggerChannelFactory->get('batch_jobs')
          ->warning('Error found @e', ['@e' => $e]);
      }
    }

    // Create the operations array for the batch.
    $batch_operations = 0;
    $batch_id = 1;

    $batch_number = 1;
    $num_operations = ceil($count / $batch_number);
    $operations = [];
    // Running through $num_operations amount of times
    // and run 100 nodes per batch per.
    for ($i = 0; $i < $num_operations; $i++) {
      // Prepare the operation. Here we could do other operations on nodes.
      $this->output()->writeln("Preparing batch: " . $batch_id);

      $operations[] = [
        '\Drupal\batch_jobs\BatchJobsService::processFieldCarousel',
        [
          $batch_id,
          $content_type,
          $nids,
          $this->t('Updating batch @batch', ['@batch' => $batch_id]),
        ],
      ];
      $batch_id++;
      $batch_operations++;
    }

    // Create the batch.
    $batch = [
      'title' => $this->t('Updating @num node(s)', ['@num' => $batch_operations]),
      'operations' => $operations,
      'finished' => '\Drupal\batch_jobs\BatchJobsService::batchJobFinished',
    ];

    $this->logger()->warning("Total Nodes: " . $count);

    // Add batch operations as new batch sets.
    batch_set($batch);

    // Process the batch sets.
    drush_backend_batch_process();

    // Log batch end notification.
    $this->logger()->notice("Batch operations end.");
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update batch operations end.');
  }

  /**
   * Prepare batch job for field_inline_gallery.
   *
   * @param string $content_type
   *   Content type to update.
   * @param string $nids
   *   Comma separated file of Node id's.
   *
   * @command update:field-inline-gallery
   * @aliases update-field-inline-gallery
   *
   * @options string $nids
   *   Comma separated file of Node id's.
   *
   * @usage update:field-inline-gallery content_type
   *   content_type is the type of node to update
   */
  public function updateFieldInlineGallery($content_type, $nids = '') {
    // Log the start of the script.
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update nodes batch operations start');

    if (!empty($nids)) {
      $count = count(explode(',', $nids));
    }
    else {
      // Retrieve all nodes of this type.
      try {
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields(NULL, ['nid']);
        $query->condition('type', $content_type);
        $count = $query->countQuery()->execute()->fetchField();
      }
      catch (\Exception $e) {
        $this->output()->writeln($e);
        $this->loggerChannelFactory->get('batch_jobs')
          ->warning('Error found @e', ['@e' => $e]);
      }
    }

    // Create the operations array for the batch.
    $batch_operations = 0;
    $batch_id = 1;

    $batch_number = 1;
    $num_operations = ceil($count / $batch_number);
    $operations = [];
    // Running through $num_operations amount of times
    // and run 100 nodes per batch per.
    for ($i = 0; $i < $num_operations; $i++) {
      // Prepare the operation. Here we could do other operations on nodes.
      $this->output()->writeln("Preparing batch: " . $batch_id);

      $operations[] = [
        '\Drupal\batch_jobs\BatchJobsService::processFieldInlineGallery',
        [
          $batch_id,
          $content_type,
          $nids,
          $this->t('Updating batch @batch', ['@batch' => $batch_id]),
        ],
      ];
      $batch_id++;
      $batch_operations++;
    }

    // Create the batch.
    $batch = [
      'title' => $this->t('Updating @num node(s)', ['@num' => $batch_operations]),
      'operations' => $operations,
      'finished' => '\Drupal\batch_jobs\BatchJobsService::batchJobFinished',
    ];

    $this->logger()->warning("Total Nodes: " . $count);

    // Add batch operations as new batch sets.
    batch_set($batch);

    // Process the batch sets.
    drush_backend_batch_process();

    // Log batch end notification.
    $this->logger()->notice("Batch operations end.");
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update batch operations end.');
  }

  /**
   * Prepare batch job for field_inline_galleries.
   *
   * @param string $content_type
   *   Content type to update.
   * @param string $nids
   *   Comma separated file of Node id's.
   *
   * @command update:field-inline-galleries
   * @aliases update-field-inline-galleries
   *
   * @options string $nids
   *   Comma separated file of Node id's.
   *
   * @usage update:field-inline-galleries content_type
   *   content_type is the type of node to update
   */
  public function updateFieldInlineGalleries($content_type, $nids = '') {
    // Log the start of the script.
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update nodes batch operations start');

    if (!empty($nids)) {
      $count = count(explode(',', $nids));
    }
    else {
      // Retrieve all nodes of this type.
      try {
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields(NULL, ['nid']);
        $query->condition('type', $content_type);
        $count = $query->countQuery()->execute()->fetchField();
      }
      catch (\Exception $e) {
        $this->output()->writeln($e);
        $this->loggerChannelFactory->get('batch_jobs')
          ->warning('Error found @e', ['@e' => $e]);
      }
    }

    // Create the operations array for the batch.
    $batch_operations = 0;
    $batch_id = 1;

    $batch_number = 1;
    $num_operations = ceil($count / $batch_number);
    $operations = [];
    // Running through $num_operations amount of times
    // and run 100 nodes per batch per.
    for ($i = 0; $i < $num_operations; $i++) {
      // Prepare the operation. Here we could do other operations on nodes.
      $this->output()->writeln("Preparing batch: " . $batch_id);

      $operations[] = [
        '\Drupal\batch_jobs\BatchJobsService::processFieldInlineGalleries',
        [
          $batch_id,
          $content_type,
          $nids,
          $this->t('Updating batch @batch', ['@batch' => $batch_id]),
        ],
      ];
      $batch_id++;
      $batch_operations++;
    }

    // Create the batch.
    $batch = [
      'title' => $this->t('Updating @num node(s)', ['@num' => $batch_operations]),
      'operations' => $operations,
      'finished' => '\Drupal\batch_jobs\BatchJobsService::batchJobFinished',
    ];

    $this->logger()->warning("Total Nodes: " . $count);

    // Add batch operations as new batch sets.
    batch_set($batch);

    // Process the batch sets.
    drush_backend_batch_process();

    // Log batch end notification.
    $this->logger()->notice("Batch operations end.");
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update batch operations end.');
  }

  /**
   * Prepare batch job to migrate file entities to media entities.
   *
   * @param string $content_type
   *   Type of node to update
   *   Argument provided to the drush command.
   * @param string $original_field
   *   Type of node to update
   *   Argument provided to the drush command.
   * @param string $media_field
   *   Type of node to update
   *   Argument provided to the drush command.
   * @param string $nids
   *   Comma separated file of Node id's.
   *
   * @command migrate-file-entities-media-entities
   * @aliases migrate-file-entities-media-entities
   *
   * @options string $nids
   *   Comma separated file of Node id's.
   *
   * @usage migrate:file-entities-media-entities content_type original_field
   *   media_field content_type is the type of node to update original_field is
   *   the field you're getting images from media_field is the new media field
   *   you're saving images into
   */
  public function migrateFileEntitiesToMediaEntities($content_type, $original_field, $media_field, $nids = '') {
    // Log the start of the script.
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update nodes batch operations start');

    // Check the type of node given as argument, if not, set article as default.
    if (strlen($content_type) == 0) {
      $content_type = 'article';
    }

    if (!empty($nids)) {
      $count = count(explode(',', $nids));
    }
    else {
      // Retrieve all nodes of this type.
      try {
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields(NULL, ['nid']);
        $query->condition('type', $content_type);
        $count = $query->countQuery()->execute()->fetchField();
      }
      catch (\Exception $e) {
        $this->output()->writeln($e);
        $this->loggerChannelFactory->get('batch_jobs')
          ->warning('Error found @e', ['@e' => $e]);
      }
    }

    // Create the operations array for the batch.
    $batch_operations = 0;
    $batch_id = 1;

    $batch_number = 1;
    $num_operations = ceil($count / $batch_number);
    $operations = [];

    // Running through $num_operations amount of times
    // and run 100 nodes per batch per.
    for ($i = 0; $i < $num_operations; $i++) {
      // Prepare the operation. Here we could do other operations on nodes.
      $this->output()->writeln("Preparing batch: " . $batch_id);

      $operations[] = [
        '\Drupal\batch_jobs\BatchJobsService::processMediaEntities',
        [
          $batch_id,
          $content_type,
          $original_field,
          $media_field,
          $nids,
          $this->t('Updating batch @batch', ['@batch' => $batch_id]),
        ],
      ];
      $batch_id++;
      $batch_operations++;
    }

    // Create the batch.
    $batch = [
      'title' => $this->t('Updating @num node(s)', ['@num' => $batch_operations]),
      'operations' => $operations,
      'finished' => '\Drupal\batch_jobs\BatchJobsService::batchJobFinished',
    ];

    $this->logger()->warning("Total Nodes: " . $count);

    // Add batch operations as new batch sets.
    batch_set($batch);

    // Process the batch sets.
    drush_backend_batch_process();

    // Log batch end notification.
    $this->logger()->notice("Batch operations end.");
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update batch operations end.');
  }

  /**
   * Prepare batch job to migrate file entities to media entities.
   *
   * @param string $content_type
   *   Type of node to update
   *   Argument provided to the drush command.
   * @param string $media_field
   *   Type of node to update
   *   Argument provided to the drush command.
   * @param string $nids
   *   Comma separated file of Node id's.
   *
   * @command update-news-listicles-items2
   * @aliases update-news-listicles-items2
   *
   * @options string $nids
   *   Comma separated file of Node id's.
   *
   * @usage update:update-news-listicles-items2 content_type media_field
   *   content_type is the type of node to update
   *   media_field is the new media field you're saving images into
   */
  public function updateNewsListicleItems2($content_type, $media_field, $nids = '') {
    // Log the start of the script.
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update nodes batch operations start');

    // Check the type of node given as argument, if not, set article as default.
    if (strlen($content_type) == 0) {
      $content_type = 'article';
    }

    if (!empty($nids)) {
      $count = count(explode(',', $nids));
    }
    else {
      // Retrieve all nodes of this type.
      try {
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields(NULL, ['nid']);
        $query->condition('type', $content_type);
        $count = $query->countQuery()->execute()->fetchField();
      }
      catch (\Exception $e) {
        $this->output()->writeln($e);
        $this->loggerChannelFactory->get('batch_jobs')
          ->warning('Error found @e', ['@e' => $e]);
      }
    }

    // Create the operations array for the batch.
    $batch_operations = 0;
    $batch_id = 1;

    $batch_number = 1;
    $num_operations = ceil($count / $batch_number);
    $operations = [];

    // Running through $num_operations amount of times
    // and run 100 nodes per batch per.
    for ($i = 0; $i < $num_operations; $i++) {
      // Prepare the operation. Here we could do other operations on nodes.
      $this->output()->writeln("Preparing batch: " . $batch_id);

      $operations[] = [
        '\Drupal\batch_jobs\BatchJobsService::processNewsListicleItems2',
        [
          $batch_id,
          $content_type,
          $media_field,
          $nids,
          $this->t('Updating batch @batch', ['@batch' => $batch_id]),
        ],
      ];
      $batch_id++;
      $batch_operations++;
    }

    // Create the batch.
    $batch = [
      'title' => $this->t('Updating @num node(s)', ['@num' => $batch_operations]),
      'operations' => $operations,
      'finished' => '\Drupal\batch_jobs\BatchJobsService::batchJobFinished',
    ];

    $this->logger()->warning("Total Nodes: " . $count);

    // Add batch operations as new batch sets.
    batch_set($batch);

    // Process the batch sets.
    drush_backend_batch_process();

    // Log batch end notification.
    $this->logger()->notice("Batch operations end.");
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update batch operations end.');
  }

  /**
   * Prepare batch job for field_items.
   *
   * @param string $content_type
   *   Content type to update.
   * @param string $nids
   *   Comma separated file of Node id's.
   *
   * @command update:field-items
   * @aliases update-field-items
   *
   * @options string $nids
   *   Comma separated file of Node id's.
   *
   * @usage update:field-items content_type
   *   content_type is the type of node to update
   */
  public function updateFieldItems($content_type, $nids = '') {
    // Log the start of the script.
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update nodes batch operations start');

    if (!empty($nids)) {
      $count = count(explode(',', $nids));
    }
    else {
      // Retrieve all nodes of this type.
      try {
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields(NULL, ['nid']);
        $query->condition('type', $content_type);
        $count = $query->countQuery()->execute()->fetchField();
      }
      catch (\Exception $e) {
        $this->output()->writeln($e);
        $this->loggerChannelFactory->get('batch_jobs')
          ->warning('Error found @e', ['@e' => $e]);
      }
    }

    // Create the operations array for the batch.
    $batch_operations = 0;
    $batch_id = 1;

    $batch_number = 1;
    $num_operations = ceil($count / $batch_number);
    $operations = [];
    // Running through $num_operations amount of times
    // and run 100 nodes per batch per.
    for ($i = 0; $i < $num_operations; $i++) {
      // Prepare the operation. Here we could do other operations on nodes.
      $this->output()->writeln("Preparing batch: " . $batch_id);

      $operations[] = [
        '\Drupal\batch_jobs\BatchJobsService::processFieldItems',
        [
          $batch_id,
          $content_type,
          $nids,
          $this->t('Updating batch @batch', ['@batch' => $batch_id]),
        ],
      ];
      $batch_id++;
      $batch_operations++;
    }

    // Create the batch.
    $batch = [
      'title' => $this->t('Updating @num node(s)', ['@num' => $batch_operations]),
      'operations' => $operations,
      'finished' => '\Drupal\batch_jobs\BatchJobsService::batchJobFinished',
    ];

    $this->logger()->warning("Total Nodes: " . $count);

    // Add batch operations as new batch sets.
    batch_set($batch);

    // Process the batch sets.
    drush_backend_batch_process();

    // Log batch end notification.
    $this->logger()->notice("Batch operations end.");
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update batch operations end.');
  }

  /**
   * Prepare batch job for processPublishedNodes.
   *
   * @param string $content_type
   *   Content type to update.
   * @param string $nids
   *   Comma separated file of Node id's.
   *
   * @command sync:node-status
   * @aliases sync-node-status
   *
   * @options string $content_type
   *   Content type to update.
   * @options string $nids
   *   Comma separated file of Node id's.
   *
   * @usage sync:node-status
   *   content_type is the type of node to update
   */
  public function syncNodeStatus($content_type = '', $nids = '') {
    // Log the start of the script.
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update nodes batch operations start');

    if (!empty($nids)) {
      $count = count(explode(',', $nids));
    }
    else {
      // Retrieve all nodes of this type.
      try {
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields(NULL, ['nid']);
        if (!empty($content_type)) {
          $query->condition('type', $content_type);
        }
        $count = $query->countQuery()->execute()->fetchField();
      }
      catch (\Exception $e) {
        $this->output()->writeln($e);
        $this->loggerChannelFactory->get('batch_jobs')
          ->warning('Error found @e', ['@e' => $e]);
      }
    }

    // Create the operations array for the batch.
    $batch_operations = 0;
    $batch_id = 1;

    $batch_number = 1;
    $num_operations = ceil($count / $batch_number);
    $operations = [];
    // Running through $num_operations amount of times
    // and run 100 nodes per batch per.
    for ($i = 0; $i < $num_operations; $i++) {
      // Prepare the operation. Here we could do other operations on nodes.
      $this->output()->writeln("Preparing batch: " . $batch_id);

      $operations[] = [
        '\Drupal\batch_jobs\BatchJobsService::syncNodesStatus',
        [
          $batch_id,
          $content_type,
          $nids,
          $this->t('Updating batch @batch', ['@batch' => $batch_id]),
        ],
      ];
      $batch_id++;
      $batch_operations++;
    }

    // Create the batch.
    $batch = [
      'title' => $this->t('Updating @num node(s)', ['@num' => $batch_operations]),
      'operations' => $operations,
      'finished' => '\Drupal\batch_jobs\BatchJobsService::batchJobFinished',
    ];

    $this->logger()->warning("Total Nodes: " . $count);

    // Add batch operations as new batch sets.
    batch_set($batch);

    // Process the batch sets.
    drush_backend_batch_process();

    // Log batch end notification.
    $this->logger()->notice("Batch operations end.");
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update batch operations end.');
  }

  /**
   * Bulk Update Content Type by running all batch jobs together.
   *
   * @param string $content_type
   *   Content type to update.
   * @param string $nids
   *   Comma separated file of Node id's.
   *
   * @command update:content-type
   * @aliases update-content-type
   *
   * @options string $content_type
   *   Content type to update.
   * @options boolean $new_nids_only
   *   Flag to identify to update new/missing nids only
   * @options string $nids
   *   Comma separated file of Node id's.
   *
   * @usage update:content-type
   *   content_type is the type of node to update
   */
  public function updateContentType($content_type, $new_nids_only = FALSE, $nids = '') {

    $this->loggerChannelFactory->get('batch_jobs')
      ->info('NEW NIDS: ' . print_r($new_nids_only, true));

    // Log the start of the script.
    if (!empty($nids)) {
      $count = count(explode(',', $nids));
    }
    else {
      // Retrieve all nodes of this type.
      try {
        // Check if we only need new/missing items.
        if ($new_nids_only == TRUE) {
          $nids = _batch_jobs_get_new_nids($content_type);
          $count = count($nids);
        }
        else {
          $db = Database::getConnection();
          $query = $db->select('node', 'n');
          $query->fields(NULL, ['nid']);
          if (!empty($content_type)) {
            $query->condition('type', $content_type);
          }
          $count = $query->countQuery()->execute()->fetchField();
        }
      } catch (\Exception $e) {
        $this->output()->writeln($e);
        $this->loggerChannelFactory->get('batch_jobs')
          ->warning('Error found @e', ['@e' => $e]);
      }
    }

    $this->loggerChannelFactory->get('batch_jobs')
      ->info('NIDS: ' . print_r($nids, true));

    // Create the operations array for the batch.
    $batch_operations = 0;
    $batch_id = 1;

    $batch_number = 1;
    $num_operations = ceil($count / $batch_number);
    $operations = [];
    // Running through $num_operations amount of times
    // and run 100 nodes per batch per.
    for ($i = 0; $i < $num_operations; $i++) {
      // Prepare the operation. Here we could do other operations on nodes.
      $this->output()->writeln("Preparing batch: " . $batch_id);

      $operations[] = [
        '\Drupal\batch_jobs\BatchJobsService::processContentType',
        [
          $batch_id,
          $content_type,
          $nids,
          $this->t('Updating batch @batch', ['@batch' => $batch_id]),
        ],
      ];
      $batch_id++;
      $batch_operations++;
    }

    // Create the batch.
    $batch = [
      'title' => $this->t('Updating @num node(s)', ['@num' => $batch_operations]),
      'operations' => $operations,
      'finished' => '\Drupal\batch_jobs\BatchJobsService::batchJobFinished',
    ];

    $this->logger()->warning("Total Nodes: " . $count);

    // Add batch operations as new batch sets.
    batch_set($batch);

    // Process the batch sets.
    drush_backend_batch_process();

    // Log batch end notification.
    $this->logger()->notice("Batch operations end.");
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update batch operations end.');
  }

  /**
   * Update News Listicle Display Format Field.
   *
   * @param string $content_type
   *   Content type to update.
   * @param string $nids
   *   Comma separated file of Node id's.
   *
   * @command update:news-listicle-display-format
   * @aliases update-news-listicle-display-format
   *
   * @options string $content_type
   *   Content type to update.
   * @options string $nids
   *   Comma separated file of Node id's.
   *
   * @usage update:news-listicle-display-format
   *   content_type is the type of node to update
   */
  public function updateNewsListicleDisplayFormat($content_type, $nids = '') {
    // Log the start of the script.
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('NIDS: ' . $nids);

    if (!empty($nids)) {
      $count = count(explode(',', $nids));
    }
    else {
      // Retrieve all nodes of this type.
      try {
        $db = Database::getConnection();
        $query = $db->select('node', 'n');
        $query->fields(NULL, ['nid']);
        if (!empty($content_type)) {
          $query->condition('type', $content_type);
        }
        $count = $query->countQuery()->execute()->fetchField();
      }
      catch (\Exception $e) {
        $this->output()->writeln($e);
        $this->loggerChannelFactory->get('batch_jobs')
          ->warning('Error found @e', ['@e' => $e]);
      }
    }

    // Create the operations array for the batch.
    $batch_operations = 0;
    $batch_id = 1;

    $batch_number = 1;
    $num_operations = ceil($count / $batch_number);
    $operations = [];
    // Running through $num_operations amount of times
    // and run 100 nodes per batch per.
    for ($i = 0; $i < $num_operations; $i++) {
      // Prepare the operation. Here we could do other operations on nodes.
      $this->output()->writeln("Preparing batch: " . $batch_id);

      $operations[] = [
        '\Drupal\batch_jobs\BatchJobsService::updateNewsListicleDisplayFormat',
        [
          $batch_id,
          $content_type,
          $nids,
          $this->t('Updating batch @batch', ['@batch' => $batch_id]),
        ],
      ];
      $batch_id++;
      $batch_operations++;
    }

    // Create the batch.
    $batch = [
      'title' => $this->t('Updating @num node(s)', ['@num' => $batch_operations]),
      'operations' => $operations,
      'finished' => '\Drupal\batch_jobs\BatchJobsService::batchJobFinished',
    ];

    $this->logger()->warning("Total Nodes: " . $count);

    // Add batch operations as new batch sets.
    batch_set($batch);

    // Process the batch sets.
    drush_backend_batch_process();

    // Log batch end notification.
    $this->logger()->notice("Batch operations end.");
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update batch operations end.');
  }

  /**
   * Prepare batch job for to download all images.
   *
   * @command get:images
   * @aliases get-images
   *
   * @usage get:images
   */
  public function downloadAllImages() {
    // Log the start of the script.
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Starting to download missing images');

    try {
      // Switch to D7 database.
      $db = _batch_jobs_connect_to_migration_db();
      $query = $db->select('file_managed', 'fm');
      $query->fields(NULL, ['uri']);
      $query->orderBy('timestamp', 'DESC');
      $count = $query->countQuery()->execute()->fetchField();

      // Switch back to the D8 DB.
      Database::setActiveConnection();
    }
    catch (\Exception $e) {
      $this->output()->writeln($e);
      $this->loggerChannelFactory->get('batch_jobs')
        ->warning('Error found @e', ['@e' => $e]);
    }

    // Create the operations array for the batch.
    $batch_operations = 0;
    $batch_id = 1;

    $batch_number = 100;
    $num_operations = ceil($count / $batch_number);
    $operations = [];

    // Running through $num_operations amount of times
    // and run 100 files per batch per.
    for ($i = 0; $i < $num_operations; $i++) {
      $this->output()->writeln("Preparing batch: " . $batch_id);

      $operations[] = [
        '\Drupal\batch_jobs\BatchJobsService::downloadMissingImages',
        [
          $batch_id,
          $this->t('Updating node @nid', ['@nid' => $batch_id]),
        ],
      ];
      $batch_id++;
      $batch_operations++;
    }

    // Create the batch.
    $batch = [
      'title' => $this->t('Updating @num node(s)', ['@num' => $num_operations]),
      'operations' => $operations,
      'finished' => '\Drupal\batch_jobs\BatchJobsService::batchJobFinished',
    ];

    // Add batch operations as new batch sets.
    batch_set($batch);

    $this->logger()->warning("Total Files: " . $count);

    // Process the batch sets.
    drush_backend_batch_process();

    // Log batch end notification.
    $this->logger()->notice("Batch operations end.");
    $this->loggerChannelFactory->get('batch_jobs')->info('Images downloaded.');
  }


  /**
   * Prepare batch job to get missing nids.
   *
   * @param string $content_type
   *   Content type to update.
   *
   * @command get:missing-nids
   * @aliases get-missing-nids
   *
   * @usage get-missing-nids content_type
   *   content_type is the type of node to update
   */
  public function getMissingNids() {
    // Log the start of the script.
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update nodes batch operations start');

    // Create the operations array for the batch.
    $batch_operations = 0;
    $batch_id = 1;

    $content_types = [
      'big_read',
      'news_article',
      'cars_road_test',
      'cars_car',
      'cars_garage',
      'videos_video',
      'news_listicle',
    ];

    $num_operations = 1;
    $operations = [];

    for ($i = 0; $i < $num_operations; $i++) {
      // Prepare the operation. Here we could do other operations on nodes.
      $this->output()->writeln("Preparing batch: " . $batch_id);

      $operations[] = [
        '\Drupal\batch_jobs\BatchJobsService::processMissingNids',
        [
          $batch_id,
          $content_types,
          $this->t('Updating batch @batch', ['@batch' => $batch_id]),
        ],
      ];
      $batch_id++;
      $batch_operations++;
    }

    // Create the batch.
    $batch = [
      'title' => $this->t('Updating @num node(s)', ['@num' => $batch_operations]),
      'operations' => $operations,
      'finished' => '\Drupal\batch_jobs\BatchJobsService::batchJobFinished',
    ];

    $this->logger()->warning("Total Content Types: " . count($content_types));

    // Add batch operations as new batch sets.
    batch_set($batch);

    // Process the batch sets.
    drush_backend_batch_process();

    // Log batch end notification.
    $this->logger()->notice("Batch operations end.");
    $this->loggerChannelFactory->get('batch_jobs')
      ->info('Update batch operations end.');
  }

}
